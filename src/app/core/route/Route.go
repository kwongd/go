package route

import (
	"net/http"

	"github.com/labstack/echo"
	tetrisController "got.kr/go/src/app/game/tetris/controller"
)

func Routing(e *echo.Echo) {
	/* static */
	e.Static("/static", "src/resources/static")

	/* index routing */
	e.GET("/", func(c echo.Context) error {
		return c.HTML(http.StatusOK, "index.html")
	})

	/* controller routing */
	tetrisController.TetrisController(e)
}
