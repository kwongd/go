package model

import (
	commonModel "got.kr/go/src/app/game/common/model"
)

type Tetris struct {
	/* common model */
	Common commonModel.Common

	/* tetris model */
	Score int `json:"score"`
	Level int `json:"level"`

	Width       int `json:"width"`       // tetris block width count
	Height      int `json:"height"`      // tetris block height count
	BlockWidth  int `json:"blockWidth"`  // tetris block width size
	BlockHeight int `json:"blockHeight"` // tetris block height size

	Blocks map[string][][]int `json:"blocks"`

	NowBlockPositionX int            `json:"nowBlockPositionX"`
	NowBlockPositionY int            `json:"nowBlockPositionY"`
	NowBlock          map[string]int `json:"nowBlock"`
	NextBlock         map[string]int `json:"nextBlock"`

	NowBlockCanvasData     map[string][][]int `json:"nowBlockCanvasData"`
	StackedBlockCanvasData map[string][][]int `json:"stackedBlockCanvasData"`
}

func DefaultInit(tetris *Tetris) *Tetris {

	tetris.Width = 10
	tetris.Height = 20

	// tetris block size init
	if tetris.Common.CtxWidth == 0 || tetris.Common.CtxHeight == 0 {
		tetris.Common.CtxWidth = 300
		tetris.Common.CtxHeight = tetris.Common.CtxWidth * tetris.Height / tetris.Width
	}
	tetris.BlockWidth = tetris.Common.CtxWidth / tetris.Width
	tetris.BlockHeight = tetris.Common.CtxHeight / tetris.Height

	tetris.Blocks = make(map[string][][]int)
	tetris.Blocks["I"] = [][]int{
		{1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0},
	}
	tetris.Blocks["O"] = [][]int{
		{0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	}
	tetris.Blocks["T"] = [][]int{
		{0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0},
		{0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0},
		{0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
	}
	tetris.Blocks["S"] = [][]int{
		{0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
	}
	tetris.Blocks["Z"] = [][]int{
		{1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
	}
	tetris.Blocks["J"] = [][]int{
		{1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
		{1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0},
	}
	tetris.Blocks["L"] = [][]int{
		{0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0},
		{1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		{1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
	}

	return tetris
}
