package main // import "got.kr/go"

import (
	"got.kr/go/src/app/core/render"
	"got.kr/go/src/app/core/route"
	"got.kr/go/src/app/core/server"
)

func main() {
	e := server.Server()

	// rendering
	e.Renderer = render.Rendering()

	// routing
	route.Routing(e)

	// startring, port set
	e.Logger.Fatal(e.Start(":1323"))
}
